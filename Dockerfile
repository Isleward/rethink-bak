FROM python:3-alpine

WORKDIR /usr/src/rethink-bak

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY ./scripts .

CMD ["crond", "-f", "-d", "8"]
