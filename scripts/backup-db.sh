#!/usr/bin/env sh
db_name=${1:-live} # Set variable to the value of 1st commandline argument or default to 'live' if not set
db_host=${RDBKP_HOST:-localhost} # Set variable to the value of RDBKP_HOST env var or default to 'localhost' if not set
db_port=${RDBKP_PORT:-28015} # Set variable to the value of RDBKP_PORT env var or default to '28015' if not set
dest_folder=${RDBKP_DEST:-~/rethinkdb_dumps} # Set variable to the value of RDBKP_DEST env var or default to '~/rethinkdb_dumps' if not set
client_count=${RDBKP_CLIENTS:-3} # Set variable to the value of RDBKP_CLIENTS env var or default to '3' if not set

dest_folder="${dest_folder}/${db_name}"
mkdir -p $dest_folder
cd $dest_folder

echo "Starting backup of $db_name"
rethinkdb-dump -c $db_host:$db_port -e $db_name --clients $client_count --quiet

if [ $? -eq 0 ] ; then
    echo "Backup finished successfully"
else
    echo "Backup failed with exit code $?"
fi

# Remove all but 72 (three days' worth of hourly backups) of the most recent files
# https://stackoverflow.com/a/25789
rm `ls -t | awk 'NR>72'`
