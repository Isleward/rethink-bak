#!/usr/bin/env sh
filename=$1
db_name=${2:-live} # Set variable to the value of 2nd commandline argument or default to 'live' if not set
db_host=${RDBKP_HOST:-localhost} # Set variable to the value of RDBKP_HOST env var or default to 'localhost' if not set
db_port=${RDBKP_PORT:-28015} # Set variable to the value of RDBKP_PORT env var or default to '28015' if not set
force_import=${3:-false} # Set variable to the value of 3nd commandline argument or default to 'false' if not set

if [ "$force_import" = true ] ; then
    rethinkdb-restore $filename -c $db_host:$db_port -i $db_name --force
else
    rethinkdb-restore $filename -c $db_host:$db_port -i $db_name
fi